import { ApiService } from './service/apiService';
import { UserModel } from './model/user.model';
import { List } from './components/list';
export const App = {
    /**
         * inititialize the application    
    */
    init() {
        this.header = document.getElementById('header');
        this.render();        
        ApiService.fetchUsers(10, 0).then(resp => {
            UserModel.data = resp;
            UserModel.fields = ['firstName', 'lastName'];
            UserModel.titles = ['First Name', 'Last Name'];
            List.Init(UserModel);
        })
    },
    /**
         * Render the application title    
    */
    render() {
        const TITLE = 'Demo Application';
        this.header.innerHTML = `
        <section class="app">
            <h3> ${TITLE} </h3>
        </section>
    `
    }
};

App.init();