const BASE = "https://hr.oat.taocloud.org/v1";

let ApiService = {
    /**
             * fetch the user list
             * @param limit
             * @param offset
      */
  async fetchUsers(limit = 20, offset = 0) {
    const USER = '/users'
    const URL = `${BASE}${USER}?limit=${limit}&offset=${offset}`;
    const response = await fetch(URL);
    return response.json();
  }
};

export { ApiService };