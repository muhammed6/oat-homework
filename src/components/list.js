export const List = {
    /**
         * inititialize the list
         * @param props    
    */
    Init(props) {
        this.appElement = document.getElementById('app');
        this.render(props);
    },
    /**
         * Render the list
         * @param data  // array of data  
         * @param titles   // array of titles 
         * @param fields    // array of fields 
    */
    render({ data, titles ,fields}) {
        let table = `<div className="table-responsive mt-3 common-table">
                        <table className="table table-bordered table-vertical table-hover" aria-label="custom pagination table">
                        <thead>
                        <tr>`;
        titles.map((title, index) => {
            table += `<th id=${index}> ${title} </th>`;
        });
        table += ` </tr>  </thead> <tbody> `;
        data.map((row, index) => {
            table += `<tr id=${index}>`;
            {
                fields.map((f, i) => {
                    table += `<td id=${i} title=${row[f]}>${row[f]}</td>`;
                });
            }
            table += `</tr>`;
        });
        table += ` </tbody>            
        </table>
    </div>`;

        this.appElement.innerHTML = table;
    }
};